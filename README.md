# const-laravel

`const-cli`模板项目，此项目为`Laravel`模板

``` shell
npm i const-cli -g
const init Project_Name laravel
```

本模板集成了：

- Laravel 6
- intervention/image

## 一、模板如何使用

### 1.1 通过cli新建项目

``` shell
const init Project_Name laravel
```

### 1.2 安装依赖

在项目根目录执行：

``` shell
composer install
php artisan key:generate
php artisan migrate
```
### 1.3 启动

方式1：`php -S localhost:8000 -t public`

方式2：`php artisan serve`

### 1.4 部署

#### 1.4.1 部署步骤

``` sh
composer install
php artisan cache:clear 
php artisan config:clear
php artisan key:generate
php artisan migrate
sudo chmod -R 755 laravel_blog
chmod -R o+w laravel_blog/storage

php artisan up # 上线
php artisan down # 下线
```
#### 1.4.2 常见问题

**Q: Do not run Composer as root/super user!**

```sh
useradd www-deploy -g www-data
passwd www-deploy  // 添加密码
su www-deploy
```

**Q: Cannot create cache directory**

```sh
sudo chown -R $USER $HOME/.composer
```

**Q: Your requirements could not be resolved to an installable set of packages.**

``` sh
composer install --ignore-platform-reqs
composer update --ignore-platform-reqs
```

## LICENSE

[MIT](LICENSE)

## 附录：

> Laravel后端问题整理: https://www.zzboy.cn/Learning/ab11027998a1
### Laravel常用命令

- 安装依赖：`composer install`
- 生成密钥：`php artisan key:generate`
- 启动本地服务：`php artisan serve`
- 创建
	- 创建数据表：`php artisan make:migration r_users --create=r_users`
	- 创建模型：`php artisan make:model RUsers`  -m 带数据表，-c 带控制器， -r 带路由
	- 创建控制器：`php artisan make:controller RUsersController`  --resource 带有Rest风格方法
	- 创建中间件：`php artisan make:middleware filterTime`
- 生成数据表：`php artisan migrate`
- 清空数据表：`php artisan migrate:refresh`  :reset 删除所有数据表
- 部署启动：`php artisan up`

**备注：**
laravel中的数据表都使用复数形式（users）
默认CRUD检索id字段为`id`，因此尽量不自定义id字段

> 更多 https://www.cnblogs.com/myzan/p/12000857.html

### 数据表范例

``` php
public function up()
{
    Schema::create('r_xxs', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('calorie', false, false)->nullable()->comment('卡路里');
        $table->bigInteger('rid')->foreign('rid')->references('rid')->on('r_users');
        $table->tinyInteger('sex')->nullable()->comment('性别:0位置1男2女');
        $table->string('openid', 50)->unique()->comment('openid');
        $table->timestamp('time_start')->nullable()->comment('开始时间'); 
        $table->timestamps(); //自动添加created_at和updated_at字段
        // $table->primary(['rid']);
    });
}
```

### 中间件范例

``` php
public function handle($request, Closure $next)
{   
    // 过滤掉时间
    $request->offsetUnset('created_at');
    $request->offsetUnset('updated_at');
    return $next($request);
}
```

``` php
//Kernel.php中注册
protected $routeMiddleware = [
    'auth' => \App\Http\Middleware\Authenticate::class,
    'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
    'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
    'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
    'can' => \Illuminate\Auth\Middleware\Authorize::class,
    'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
    'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
    'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
    'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,
    // 自定义中间件
    'filterTime' => \App\Http\Middleware\filterTime::class
];
```

``` php
//路由中使用
Route::prefix('user')->group(function () {
	Route::post('/doUpdate', 'RUsersController@doUpdate')->middleware('filterTime');
});
```
