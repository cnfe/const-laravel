<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CAdmins extends Model
{
    // 可添加字段
    protected $fillable = [
        "phone",
        "password",
        "token"
    ];

    /**
     * 更新token
     * @return mixed|string
     */
    public function generateToken() {
        $this->token = '#edu#'.randStr(42);
        $this->save();
        return $this->token;
    }
}
