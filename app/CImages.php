<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CImages extends Model
{
    // 可添加字段
    protected $fillable = [
        "name",
        "oname",
        "store",
        "extension",
        "mimetype",
        "size",
        "width",
        "height",
        "mwidth",
        "mheight",
        "original",
        "thumbnail",
        "error"
    ];
}
