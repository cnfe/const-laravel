<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CNews extends Model
{
    // 可添加字段
    protected $fillable = [
        "title",
        "subtitle",
        "about",
        "image"
    ];
}
