<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CUsers extends Model
{
    // 可添加字段
    protected $fillable = [
        "type",
        "openid",
        "name",
        "sex",
        "phone",
        "idcard",
        "age",
        "edu",
        "major",
        "schoolid",
        "majorid",
        "idcardid",
        "photoid",
        "certid",
        "paytype",
        "payid",
        "steps"
    ];
}
