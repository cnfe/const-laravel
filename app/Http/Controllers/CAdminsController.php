<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\CAdmins;
use App\CUsers;
use App\CNews;

class CAdminsController extends Controller
{
    /**
     * 新增管理员
     */
    public function add(Request $request) {
        $admin = new CAdmins();
        if ($request->has('super') && $request->super == env('SUPER_KEY')) {
            try {
                DB::beginTransaction();
                    // $admin->fillable(array_keys($request->all()));
                    $admin->fill([
                        'phone' => $request->phone,
                        'password' => md5($request->password),
                        'token' => '',
                    ]);
                    $admin->save();
                DB::commit();
                $data = CAdmins::where('id', $admin->id)->first();
                return returnData(true, '操作成功', $data);
            } catch (\Throwable $th) {
                DB::rollBack();
                return returnData(false, $th);
            }
        }
    }

    /**
     * 删除管理员
     */
    public function del(Request $request) {
        if ($request->has('phone') && $request->has('super') && $request->super == env('SUPER_KEY')) {
            try {
                DB::beginTransaction();
                    CAdmins::where('phone', $request->phone)->delete();
                DB::commit();
                return returnData(true, '操作成功', null);
            } catch (\Throwable $th) {
                DB::rollBack();
                return returnData(false, $th);
            }
        }else{
            return returnData(false, '缺少phone');
        }
    }

    /**
     * 修改
     */
    public function edit(Request $request){
        if ($request->has('phone') && $request->has('super') && $request->super == env('SUPER_KEY')) {
            try {
                $admin = CAdmins::where('phone', $request->phone)->first();// 获取信息
                if ($admin) {
                    DB::beginTransaction();
                        $admin->update([
                            'password' => md5($request->password),
                            'token' => '',
                        ]);
                    DB::commit();
                    $data = CAdmins::where('id', $admin->id)->first();
                    return returnData(true, '操作成功', $data);
                } else {
                    return returnData(false, '不存在', null);
                }
            } catch (\Throwable $th) {
                DB::rollBack();
                return returnData(false, $th);
            }
        }else{
            return returnData(false, '缺少phone');
        }
    }

    /**
     * 登录
     */
    public function login(Request $request) {
        if ($request->has('phone') && $request->has('password')) {
            try {
                $admin = CAdmins::where('phone', $request->phone)->first();
                if ($admin->password == md5($request->password)) {
                    $admin->generateToken();
                    DB::beginTransaction();
                        $admin->update([
                            'token' => $admin->token,
                        ]);
                    DB::commit();
                    return returnData(true, '操作成功', [
                        'token' => $admin->token,
                    ]);
                } else {
                    return returnData(false, '账号或者密码错误');
                }
            } catch (\Throwable $th) {
                DB::rollBack();
                return returnData(false, $th);
            }
        }else{
            return returnData(false, '缺少phone或者password');
        }
    }

    /**
     * 登出
     */
    public function logout(Request $request) {
        if ($request->has('token')) {
            try {
                $admin = CAdmins::where('token', $request->token)->first();
                if ($admin) {
                    $admin->generateToken();
                    DB::beginTransaction();
                        $admin->update([
                            'token' => $admin->token,
                        ]);
                    DB::commit();
                    return returnData(true, '操作成功', null);
                } else {
                    return returnData(false, 'token错误');
                }
            } catch (\Throwable $th) {
                DB::rollBack();
                return returnData(false, $th);
            }
        }else{
            return returnData(false, '缺少token');
        }
    }

    /**
     * 修改密码
     */
    public function reset(Request $request) {
        if ($request->has('token') && $request->has('password') && $request->has('newpassword')) {
            try {
                $admin = CAdmins::where('token', $request->token)->first();
                if ($admin) {
                    if ($admin->password == md5($request->password)) {
                        DB::beginTransaction();
                            $admin->update([
                                'password' => md5($request->newpassword),
                            ]);
                        DB::commit();
                        return returnData(true, '操作成功', null);
                    } else {
                        return returnData(false, '旧密码错误');
                    }
                } else {
                    return returnData(false, 'token错误');
                }
            } catch (\Throwable $th) {
                DB::rollBack();
                return returnData(false, $th);
            }
        } else {
            return returnData(false, '缺少token或者password、newpassword');
        }
    }

    /**
     * 获取系统概要信息
     */
    public function summary(Request $request) {
        $users = CUsers::count();
        $usersCheck = CUsers::where('steps', '=', 2)->count(); // 0填表1等待预付款2等待审核3报名完成
        $usersPay = CUsers::where('steps', '=', 1)->count(); // 0填表1等待预付款2等待审核3报名完成
        $news = CNews::count();
        $schools = CSchools::count();
        $majors = CMajors::count();

        $data = [
            'num' => [
                'user' => $users, // 学生总数
                'usercheck' => $usersCheck, // 待审核
                'userpay' => $usersPay, // 待支付
                'news' => $news,
                'school' => $schools,
                'major' => $majors,
            ],
            'system' => [
                // 'mem_usage' => $this->getSystemMemInfo(), // macOS不支持
                // 'disk' => [
                //     'free' => $this->getSystemDiskInfo(), // GB
                //     'total' => '', //
                //     'usage' => '',
                // ],
            ],
        ];
        return returnData(true, '操作成功', $data);
    }
    // 获取系统内存信息
    private function getSystemMemInfo() {
        $data = explode("\n", file_get_contents("/proc/meminfo"));
        $meminfo = array();
        foreach ($data as $line) {
            list($key, $val) = explode(":", $line);
            $meminfo[$key] = trim($val);
        }
        return $meminfo;
    }
    // 获取系统磁盘可用量
    private function getSystemDiskInfo() {
        $df = disk_free_space("/") / 1024 / 1024 / 1024;
        return [
            'free' => $df,
        ];
    }
}
