<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\CNews;
use App\Http\Controllers\PublicController as PublicCtr;

class CNewsController extends Controller
{
    /**
     * 新增
     */
    public function add(Request $request){
        $new = new CNews();
        try {
            DB::beginTransaction();
                $new->fillable(array_keys($request->all()));
                $new->fill($request->all());
                $new->save();
            DB::commit();
            $data = CNews::where('id', $new->id)->first();
            $data['imageimg'] = generateImageUrl($data->image); // 图片url
            return returnData(true, '操作成功', $data);
        } catch (\Throwable $th) {
            DB::rollBack();
            return returnData(false, $th);
        }
    }

    /**
     * 修改
     */
    public function edit(Request $request){
        if ($request->has('id')) {
            try {
                $new = CNews::where('id', $request->id)->first();// 获取信息
                if ($new) {
                    DB::beginTransaction();
                        $new->update($request->all());
                    DB::commit();
                    $data = CNews::where('id', $new->id)->first();
                    $data['imageimg'] = generateImageUrl($data->image); // 图片url
                    return returnData(true, '操作成功', $data);
                } else {
                    return returnData(false, '不存在', null);
                }
            } catch (\Throwable $th) {
                DB::rollBack();
                return returnData(false, $th);
            }
        }else{
            return returnData(false, '缺少id');
        }
    }

    /**
     * 查询
     */
    public function getById(Request $request){
        if ($request->has('id')) {
            try {
                $data = CNews::where('id', $request->id)->first();
                $data['imageimg'] = generateImageUrl($data->image); // 图片url
                return returnData(true, '操作成功', $data);
            } catch (\Throwable $th) {
                return returnData(false, $th);
            }
        }else{
            return returnData(false, '缺少id');
        }
    }

    /**
     * 删除
     */
    public function del(Request $request){
        if ($request->has('id')) {
            try {
                DB::beginTransaction();
                    $data = CNews::where('id', $request->id)->first();
                    PublicCtr::delImageStatic($data->image);
                    CNews::where('id', $request->id)->delete();
                DB::commit();
                return returnData(true, '操作成功', null);
            } catch (\Throwable $th) {
                DB::rollBack();
                return returnData(false, $th);
            }
        }else{
            return returnData(false, '缺少id');
        }
    }

    /**
     * 获取全部
     */
    public function all(Request $request){
        $request->has('pageindex') ? $pageindex = $request->pageindex : $pageindex = 1;  //当前页 1,2,3,...,首次查询可以传0
        $request->has('pagesize') ? $pagesize = $request->pagesize : $pagesize = 10;  //页面大小
        $request->has('keyword') ? $keyword = $request->keyword : $keyword = '';  // 搜索词
        try {
            DB::beginTransaction();
                $sql = CNews::orderBy('created_at', 'desc')
                    ->where('c_news.title', 'like', "%$keyword%")
                    ->orWhere('c_news.subtitle', 'like', "%$keyword%");
                $count = $sql->count();
                $news = $sql->skip(($pageindex - 1) * $pagesize)
                    ->take($pagesize)
                    ->get();
            DB::commit();

            $data = [];
            foreach ($news as $new) {
                $new['imageimg'] = generateImageUrl($new->image); // 图片url
                $data[] = $new;
            }
            return returnData(true, '操作成功', [
                'pageindex' => $pageindex,
                'pagesize' => $pagesize,
                'total' => $count,
                'list' => $data
            ]);
        } catch (\Throwable $th) {
            DB::rollBack();
            return returnData(false, $th);
        }
    }
}
