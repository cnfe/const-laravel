<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use File;
use Image;
use App\CImages;
use App\CNews;
use App\Http\Controllers\PublicController as PublicCtr;

class ExampleController extends Controller
{

    /**
     * 新增
     */
    public function add(Request $request){
        $new = new CNews();
        try {
            DB::beginTransaction();
                $new->fillable(array_keys($request->all()));
                $new->fill($request->all());
                $new->save();
            DB::commit();
            $data = CNews::where('id', $new->id)->first();
            $data['imageimg'] = generateImageUrl($data->image); // 图片url
            return returnData(true, '操作成功', $data);
        } catch (\Throwable $th) {
            DB::rollBack();
            return returnData(false, $th);
        }
    }

    /**
     * 修改
     */
    public function edit(Request $request){
        if ($request->has('id')) {
            try {
                $new = CNews::where('id', $request->id)->first();// 获取信息
                if ($new) {
                    DB::beginTransaction();
                        $new->update($request->all());
                    DB::commit();
                    $data = CNews::where('id', $new->id)->first();
                    $data['imageimg'] = generateImageUrl($data->image); // 图片url
                    return returnData(true, '操作成功', $data);
                } else {
                    return returnData(false, '不存在', null);
                }
            } catch (\Throwable $th) {
                DB::rollBack();
                return returnData(false, $th);
            }
        }else{
            return returnData(false, '缺少id');
        }
    }

    /**
     * 查询
     */
    public function getById(Request $request){
        if ($request->has('id')) {
            try {
                $data = CNews::where('id', $request->id)->first();
                $data['imageimg'] = generateImageUrl($data->image); // 图片url
                return returnData(true, '操作成功', $data);
            } catch (\Throwable $th) {
                return returnData(false, $th);
            }
        }else{
            return returnData(false, '缺少id');
        }
    }

    /**
     * 联表查询：查询个人信息
     */
    public function getUserById(Request $request){
        if ($request->has('openid')) {
            try {
                // 获取用户基本信息
                $data = CUsers::leftJoin('c_schools', 'c_users.schoolid', '=', 'c_schools.id')
                    ->leftJoin('c_majors', 'c_users.majorid', '=', 'c_majors.id')
                    ->select('c_users.*', 'c_schools.name as schoolname', 'c_majors.name as majorname')
                    ->where('openid', $request->openid)->first();
                if ($data) {
                    // 性别
                    $data['sexname'] = $data->sex == 1 ? '男' : '女'; // 性别中文
                    if ($data->payid) {
                        $data['payimg'] = generateImageUrl($data->payid); // 支付凭证图片url
                    } else {
                        $data['payimg'] = ['thumbnail' => '', 'original' => ''];
                    }
                }
                return returnData(true, '操作成功', $data);
            } catch (\Throwable $th) {
                return returnData(false, $th);
            }
        }else{
            return returnData(false, '缺少openid');
        }
    }

    /**
     * 删除
     */
    public function del(Request $request){
        if ($request->has('id')) {
            try {
                DB::beginTransaction();
                    $data = CNews::where('id', $request->id)->first();
                    PublicCtr::delImageStatic($data->image);
                    CNews::where('id', $request->id)->delete();
                DB::commit();
                return returnData(true, '操作成功', null);
            } catch (\Throwable $th) {
                DB::rollBack();
                return returnData(false, $th);
            }
        }else{
            return returnData(false, '缺少id');
        }
    }

    /**
     * 获取全部
     */
    public function all(Request $request){
        $request->has('pageindex') ? $pageindex = $request->pageindex : $pageindex = 1;  //当前页 1,2,3,...,首次查询可以传0
        $request->has('pagesize') ? $pagesize = $request->pagesize : $pagesize = 10;  //页面大小
        $request->has('keyword') ? $keyword = $request->keyword : $keyword = '';  // 搜索词
        try {
            DB::beginTransaction();
                $sql = CNews::orderBy('created_at', 'desc')
                    ->where('c_news.title', 'like', "%$keyword%")
                    ->orWhere('c_news.subtitle', 'like', "%$keyword%");
                $count = $sql->count();
                $news = $sql->skip(($pageindex - 1) * $pagesize)
                    ->take($pagesize)
                    ->get();
            DB::commit();

            $data = [];
            foreach ($news as $new) {
                $new['imageimg'] = generateImageUrl($new->image); // 图片url
                $data[] = $new;
            }
            return returnData(true, '操作成功', [
                'pageindex' => $pageindex,
                'pagesize' => $pagesize,
                'total' => $count,
                'list' => $data
            ]);
        } catch (\Throwable $th) {
            DB::rollBack();
            return returnData(false, $th);
        }
    }

    /** ---------------- 文件 ------------------ */

    // 图片上传
    public function uploadImg(Request $request) {
        $inputData = $request->all();
        $rules = [
            'img' => [ 'file','image','max:10240' ]
        ];
        $validator = Validator::make($inputData,$rules);
        if($validator->fails()){
            return returnData(false, "校验失败", $validator);
        }
        $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
        $photo = $inputData['img'];
        $file_name = randId();
        $date = date('Y-m-d');
        $file_relative_path = 'resources/images/'.$date;
        $file_path = public_path($file_relative_path);
        try {
            if (!is_dir($file_path)){
                mkdir($file_path);
            }
            // 保存缩略图 resources/images/2020-01-31/5e3319bcafcae-min-200x356.jpg
            $dfile = Image::make($photo);
            $min_file_path = '/'.$file_name.'.min.'.$photo->getClientOriginalExtension();
            // $min_file_path = '/'.$file_name.'-min-'.'200x'.round(200*$dfile->height()/$dfile->width()).'.'.$photo->getClientOriginalExtension();
            $image = Image::make($photo)->resize(200, null, function ($constraint) {$constraint->aspectRatio();})->save($file_path.$min_file_path);

            // 保存原图   resources/images/2020-01-31/5e3319bcafcae-1080x1920.jpg
            // $original_file_path = '/'.$file_name.'-'.$dfile->width().'x'.$dfile->height().'.'.$photo->getClientOriginalExtension();
            // $image = Image::make($photo)->save($file_path.$original_file_path);  //不能原图保存，服务器带宽获取太慢
            $original_file_path = '/'.$file_name.'.'.$photo->getClientOriginalExtension();
            // $original_file_path = '/'.$file_name.'-'.'1080x'.round(1080*$dfile->height()/$dfile->width()).'.'.$photo->getClientOriginalExtension();
            $image = Image::make($photo)->resize(1080, null, function ($constraint) {$constraint->aspectRatio();})->save($file_path.$original_file_path);
            //处理返回网络url
            $imgUrl = $protocol.$request->server('HTTP_HOST').'/'.$file_relative_path;
            $data = [
                'name' => $photo->getClientOriginalName(),
                'oname' => $photo->getClientOriginalName(),
                'store' => $date.'/'.$file_name.'#'.$photo->getClientOriginalExtension(),
                'extension' => $photo->getClientOriginalExtension(),
                'mimetype' => $photo->getClientMimeType(),
                'size' => $photo->getClientSize(),
                'width' => $dfile->width(),
                'height' => $dfile->height(),
                'mwidth' => 200,
                'mheight' => round(200*$dfile->height()/$dfile->width()),
                // 'original' => $imgUrl.$original_file_path,
                // 'thumbnail' => $imgUrl.$min_file_path,
                'original' => $file_relative_path.$original_file_path,
                'thumbnail' => $file_relative_path.$min_file_path,
                'error' => $photo->getError()
            ];

            // 存入数据库
            $cimage = new CImages();
            DB::beginTransaction();
                $cimage->fill($data);
                $cimage->save();
            DB::commit();
            $data = CImages::where('id', $cimage->id)->first();
            unset($data['id']); // 不出现id
            return returnData(true, '上传成功', $data);
        } catch (\Throwable $th) {
            DB::rollBack();
            return returnData(false, $th);
        }
    }

    /**
     * 方法：删除图片
     */
    public static function delImageStatic($store) {
        if ($store) {
             try {
                 // 删除数据记录
                 DB::beginTransaction();
                     $image = CImages::where('store', $store)->first();
                     $image_path = generateImageUrl($image->store);
                     CImages::where('store', $store)->delete();
                 DB::commit();
                 // 删除文件
                 // 重要：必须判断路径后缀是否是图片后缀，否则可能会被删除整个目录
                 if(File::exists($image_path['original']) && preg_match('/\.\w{3,4}$/', $image_path['original'])) {
                     File::delete($image_path['original']);
                 }
                 if(File::exists($image_path['thumbnail']) && preg_match('/\.\w{3,4}$/', $image_path['thumbnail'])) {
                     File::delete($image_path['thumbnail']);
                 }
                 return true;
             } catch (\Throwable $th) {
                 DB::rollBack();
                 return false;
             }
         }else{
             return false;
         }
     }

    /** ---------------- 微信 ------------------ */

    // 获取openid
    public function getOpenid(Request $request) {
        if($request->has('code')){
            $url = 'https://api.weixin.qq.com/sns/jscode2session?appid='.env('WX_APPID').'&secret='.env('WX_SECRET').'&js_code='.$request->code.'&grant_type=authorization_code';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);//不验证
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);//不验证主机
            $returnjson=curl_exec($curl);
            if($returnjson){
                //整理返回数据
                $json = json_decode($returnjson);
                if(!property_exists($json, 'errmsg')){
                    return returnData(true, "操作成功", $json);
                }else{
                    return returnData(false, $json->errmsg, null);
                }
            }else{
                return returnData(false, curl_error($curl), null);
            }
        }else{
            return returnData(false, "缺少code", null);
        }
    }
}
