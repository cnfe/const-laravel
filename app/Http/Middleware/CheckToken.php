<?php

namespace App\Http\Middleware;

use Closure;
use App\CAdmins;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // 校验管理token
        if (!$request->has('token')) {
            return response(returnData(false, '缺少token'), 403);
        }

        $admin = CAdmins::where('token', $request->token)->first();
        if (!$admin) {
            return response(returnData(false, '非法操作'), 403);
        }

        return $next($request);
    }
}
