<?php

function returnData(){
    $args = func_get_args();
    $isSuccess = $args[0]; // 必需：成功否 boolean
    $msg = $args[1]; // 必需：提示信息  anytype
    $data = count($args)>2 ? $args[2] : $args[1]; // 可选：数据，失败时可不传（使用提示信息）

    $msg = is_string($msg) ? $msg : json_encode($msg);
    return [
        'isSuccess' => $isSuccess,
        'msg' => $msg,
        'data' => $data
    ];
}

/**
 * 生成随机字符串
 */
function randStr($length) {
    $str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890~!@#$%^*<>{}[]';
    $randStr = str_shuffle($str); // 打乱字符串
    // $rands= substr($randStr,0,$length); // substr(string,start,length);返回字符串的一部分
    $len = strlen($randStr) - 1;
    $rands = '';
    for ($i = 0; $i < $length; $i++) {
        $num = mt_rand(0, $len);
        $rands .= $str[$num];
    }
    return $rands;
}

/**
 * 生成随机id
 */
function randId() {
    $length = 12;
    $pre = uniqid(); // 基于以微秒计的当前时间，生成一个唯一的 ID 13位
    $str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
    $randStr = str_shuffle($str); // 打乱字符串
    $len = strlen($randStr) - 1;
    $rands = '';
    for ($i = 0; $i < $length; $i++) {
        $num = mt_rand(0, $len);
        $rands .= $str[$num];
    }
    return $pre.$rands;
}

/**
 * 根据图片id生成url
 */
function generateImageUrl($id) {
    return [
        'original' => 'resources/images/'.str_replace('#', '.', $id),
        'thumbnail' => 'resources/images/'.str_replace('#', '.min.', $id),
    ];
}

/**
 * 获取当前状态
 */
function getStepsName($steps) {
    $stepsNames = ['待填写', '待付款', '待审核', '已交费'];
    return $stepsNames[$steps];
}
