<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_users', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned()->comment('学号');
            $table->string('type', 20)->nullable()->comment('报考类型：自考、成考、网络远程教育、国开大学、文科班、理科班');
            $table->string('openid', 50)->unique()->comment('微信id');
            $table->string('name', 20)->comment('姓名');
            $table->tinyInteger('sex')->unsigned()->comment('性别1男0女');
            $table->string('phone', 20)->comment('电话');
            $table->string('idcard', 50)->comment('身份证号');
            $table->integer('age')->comment('年龄');
            $table->string('edu', 50)->comment('当前学历');
            $table->string('major', 50)->comment('当前专业');
            $table->integer('schoolid')->unsigned()->nullable()->comment('报考院校id');
            $table->integer('majorid')->unsigned()->nullable()->comment('报考专业id');
            $table->string('idcardid', 50)->nullable()->comment('身份证 图id');
            $table->string('photoid', 50)->nullable()->comment('蓝底寸照 图id');
            $table->string('certid', 50)->nullable()->comment('毕业证照 图id');
            $table->tinyInteger('paytype')->unsigned()->default(1)->comment('支付方式，1微信0支付宝');
            $table->string('payid', 50)->nullable()->comment('支付凭证截 图id');
            $table->tinyInteger('steps')->unsigned()->default(0)->comment('当前报名进度：0填表1等待预付款2等待审核3报名完成');
            $table->timestamps();
        });
        DB::update('ALTER TABLE c_users AUTO_INCREMENT = 100000;'); // 设置学号起始值
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_users');
    }
}
