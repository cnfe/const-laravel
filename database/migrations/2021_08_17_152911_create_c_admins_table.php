<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_admins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('phone', 20)->comment('手机号');
            $table->string('password', 200)->comment('密码');
            $table->string('token', 50)->comment('token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_admins');
    }
}
