<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 50)->comment('标题');
            $table->text('subtitle')->nullable()->comment('概要');
            $table->text('about')->nullable()->comment('简介');
            $table->string('image', 50)->nullable()->comment('图片id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_news');
    }
}
