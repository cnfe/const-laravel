<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
        <!-- 360 使用Google Chrome Frame -->
        <meta name="renderer" content="webkit">
        <title>const-laravel</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="./img/favicon.ico"/>
        <!-- 清除默认样式 -->
        <link rel="stylesheet" href="css/reset.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <div class="title">Template of <span>laravel</span></div>
        <p class="console">npm i const-cli -g</p>
        <p class="console">const init Project_Name laravel</p>
    </body>
</html>
