<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * 全局接口
 */
Route::prefix('main')->group(function () {
    // 获取用户openid
    Route::get('/getOpenid', 'PublicController@getOpenid');
    // 图片上传
    Route::post('/upload', 'PublicController@uploadImg');
    // 图片地址查询
    Route::get('/image', 'PublicController@getImage');
    // 删除图片
    Route::post('/delImage', 'PublicController@delImage');
});

/**
 * 资讯表
 */
Route::prefix('news')->group(function () {
    // 新增
    Route::post('/add', 'ExampleController@add')->middleware('checkToken');
    // 查询
    Route::get('/getById', 'ExampleController@getById');
    // 修改
    Route::post('/edit', 'ExampleController@edit')->middleware('checkToken');
    // 删除
    Route::get('/del', 'ExampleController@del')->middleware('checkToken');
    // 全部
    Route::get('/all', 'ExampleController@all');
});


/**
 * 管理表
 */
Route::prefix('admin')->group(function () {
    // 新增管理员
    Route::post('/add', 'CAdminsController@add');
    // 删除管理员
    Route::post('/del', 'CAdminsController@del');
    // 重置管理员密码
    Route::post('/edit', 'CAdminsController@edit');
    // 登录
    Route::post('/login', 'CAdminsController@login');
    // 登出
    Route::post('/logout', 'CAdminsController@logout');
    // 修改密码
    Route::post('/reset', 'CAdminsController@reset');

    // 概况
    Route::get('/summary', 'CAdminsController@summary')->middleware('checkToken');

});
